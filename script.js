// const maFunction = function () {
//     alert("Hello maFunction");
// }
// maFunction();

// //creation de variable
// let texte;
// const message = function(){
//     texte = ("bonjour texte");
// }
// message();
// console.log(texte);

// //fonction avec parametre et texte par defaut
// const parametre = function (param = 'texte par defaut') {
//     alert(param);
// }
// parametre(); // affiche texte par defaut
// parametre("je suis le texte avec le parametre");


// //Les functions flechées
// const functFlechee = () => {
//     alert('hello la function flechée');
// }
// functFlechee();

// //function flechée avec parametre
// const somme = (a , b) => {
//     alert('resultat '+ (a + b));
// }

// somme(10 , 5);


//EXO1: 

function resultat(){
    let a = document.getElementById('a').value;
    let b = document.getElementById('b').value;
    let produit;

    produit = a * b ;  

    

    if (!isNaN(a) && !isNaN(b)) {
        document.getElementById('solution').innerHTML = `la multiplication de ${a} et ${b} est :` + produit ;
    } else {
        document.getElementById('solution').innerHTML = `Vous devez saisir des nombres`;
    }

}


// //EXO2 : 

document.getElementById('resultat').addEventListener("click", maFunction);

function maFunction(){
    let n = parseInt(document.getElementById('n').value);

    let modulo = n % 4 ;

    if (isNaN(n)) {
        document.getElementById('affichage').innerHTML = `Vous devez saisir un nombre`
    } else {
        document.getElementById('affichage').innerHTML = `le modulo de ${n} par 4 est : ` + modulo;
    }

}
